## Requirements

- NodeJS ^4

## Install the dependencies

>`$ npm install`

## Run the server

>`$ node app`
