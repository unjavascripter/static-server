'use strict';

const express = require('express');
const app = express();

// CORS
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods',' GET,PUT,POST,DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});


var __publicFolder = __dirname + '/public';
app.use(express.static(__publicFolder));

// API
app.get('/', function (req, res) {
    res.sendFile(path.join(__publicFolder + '/index.html'));
});

// Running the server
app.listen(3000, _ => console.log('API running on port 3000'));